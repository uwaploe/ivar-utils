#!/usr/bin/env python
#
import os
import click
import gcl
from pyarms.config import Rcvr, load_config


@click.command()
@click.option('-v', '--verbose', is_flag=True)
@click.argument('cfgfile', type=click.File('rb'))
def cli(verbose, cfgfile):
    """
    Verify an IVAR configuration file. Exit status is 0
    if ok, non-zero on error.
    """
    status = 0
    path = os.environ.get('CFGPATH',
                          os.path.expanduser('~/config')).split(':')
    try:
        cfg = load_config(cfgfile, path=path)
    except Exception as e:
        status = str(e) if verbose else 1
    else:
        try:
            rcvr = Rcvr(**(gcl.util.to_python(cfg['sample']['rcvr'])))
        except Exception as e:
            status = str(e) if verbose else 2
        else:
            if not rcvr.enable:
                status = 'ADC not enabled' if verbose else 3
    raise SystemExit(status)


if __name__ == '__main__':
    cli()
