#!/usr/bin/env bash
#

SVC="streamadc"
CFGFILE=$HOME/config/deployment.cfg
stout=$(tput smso || tput so)
estout=$(tput rmso || tput se)
bold=$(tput bold || tput md)
default=$(tput sgr0)

showstate() {
    state="$(systemctl --user is-active $SVC)"
    if [[ "$state" = "inactive" ]]; then
        state="$bold$state"
    fi
    echo "${stout}[data collection state: $state]${estout}${default}"
}

sysstatus() {
    local cmd
    while true; do
        echo "${stout}== Data Collection Status ==${estout}"
        showstate
        echo "short) view status summary"
        echo "log)   view log"
        echo "files) data file count"
        echo "space) check disk space"
        echo "q)     return to main menu"
        while true; do
            read -e -p "> " cmd
            case "$cmd" in
                short)
                    echo "Type 'q' to quit"
                    systemctl --user status $SVC
                    break
                    ;;
                log)
                    echo "Type 'q' to quit, <space> to page"
                    journalctl -r --user --user-unit $SVC
                    break
                    ;;
                files)
                    echo "${bold}$(find /IVAR/data -name '*.h5' | wc -l)${default}"
                    break
                    ;;
                space)
                    echo "${bold}$(df -h /IVAR)${default}"
                    break
                    ;;
                q)
                    return
                    ;;
                *)
                    echo "Invalid command"
                    ;;
            esac
        done
    done
}

safe_edit() {
    local msg code
    cp $CFGFILE ${CFGFILE}.bak
    nano $CFGFILE
    echo -n "Checking configuration file ... "
    msg=$(checkcfg.py -v $CFGFILE)
    code=$?
    echo
    if [[ $code != 0 ]]; then
        echo "$msg"
        echo "${bold}Invalid configuration, restoring previous contents${default}"
        mv ${CFGFILE}.bak $CFGFILE
    fi
}

editcfg() {
    if systemctl --quiet --user is-active $SVC; then
        echo "${bold}Stopping data collection ...${default}"
        systemctl --user stop $SVC
        safe_edit
        echo "${bold}Resuming data collection ...${default}"
        systemctl --user start $SVC
    else
        safe_edit
    fi
}

main() {
    local cmd
    while true; do
        echo "${stout}== Main Menu ==${estout}"
        showstate
        echo "start) start data collection"
        echo "stop)  stop data collection"
        echo "stat)  check status"
        echo "edit)  edit configuration"
        echo "q)     exit to shell prompt"
        while true; do
            read -e -p "> " cmd
            case "$cmd" in
                start)
                    systemctl --user start $SVC
                    break
                    ;;
                stop)
                    systemctl --user stop $SVC
                    break
                    ;;
                stat)
                    sysstatus
                    break
                    ;;
                edit)
                    editcfg
                    break
                    ;;
                q)
                    exit 0
                    ;;
                *)
                    echo "Invalid command."
                    ;;
            esac
        done
    done
}

main
