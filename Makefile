
CFGDIR = $(HOME)/config
BINDIR = $(HOME)/bin
SVCDIR = $(HOME)/.config/systemd/user

SVCS := streamadc.service
CFGFILE := deployment.cfg
PROGS := stopstream.sh venv_helper.sh sysmenu.sh \
	checkcfg.py


install: install-bin install-cfg install-sv

install-bin: $(PROGS)
	install -d $(BINDIR)
	install -b -m 755 -t $(BINDIR) $(PROGS)
	-rm -f $(BINDIR)/sysmenu
	ln -s $(BINDIR)/sysmenu.sh $(BINDIR)/sysmenu

install-cfg: $(CFGFILE)
	install -d $(CFGDIR)
	install -b -m 644 -t $(CFGDIR) $(CFGFILE)

install-sv: $(SVCS)
	install -d $(SVCDIR)
	install -b -m 644 -t $(SVCDIR) $^
	systemctl --user enable $(SVCS)