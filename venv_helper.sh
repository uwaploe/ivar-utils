#!/usr/bin/env bash
#
# Run a program in the Python virtualenv
#
. $HOME/venv/bin/activate
exec "$@"
