#!/usr/bin/env bash
#
# Stop the streamadc process cleanly.
#

# Send the 'halt' message
mosquitto_pub -q 1 -t adc/commands -m halt

# Wait for confirmation, allow 15 seconds
sleep 15 && kill -ALRM $$ 2> /dev/null &
alarm_pid=$!
mosquitto_sub -q 1 -C 1 -t adc/events/done
kill $alarm_pid 2> /dev/null
